/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp.node;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.gitlab.multicia.cpp.type.CppType;
import io.gitlab.multicia.graph.GraphException;
import io.gitlab.multicia.graph.GraphReader;
import io.gitlab.multicia.graph.GraphWriter;
import io.gitlab.multicia.graph.Node;
import io.gitlab.multicia.graph.NodeDiffer;
import io.gitlab.multicia.graph.NodeHasher;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.gitlab.multicia.graph.GraphReader.expectArray;
import static io.gitlab.multicia.graph.GraphReader.expectNullableArray;
import static io.gitlab.multicia.graph.GraphReader.expectNullableBoolean;

/**
 * Class
 */
public final class CppClassNode extends CppNode {
	private final @NotNull List<CppType> bases = new ArrayList<>();
	private boolean anonymous = false;


	public CppClassNode(@NotNull CppGroup group) {
		super(group);
	}

	@Override
	protected @NotNull String getNodeClass() {
		return CppGroup.CPP_CLASS_NODE;
	}


	public @NotNull List<CppType> getBases() {
		return Collections.unmodifiableList(bases);
	}

	public void setBases(@NotNull List<CppType> bases) {
		this.bases.clear();
		this.bases.addAll(bases);
	}

	public boolean isAnonymous() {
		return anonymous;
	}

	public void setAnonymous(boolean anonymous) {
		this.anonymous = anonymous;
	}


	@Override
	protected void serialize(@NotNull GraphWriter writer) throws IOException {
		super.serialize(writer);

		if (!bases.isEmpty()) {
			writer.name("bases");
			writer.beginArray();
			for (final CppType type : bases) {
				writer.writeLink(type);
			}
			writer.endArray();
		}

		if (anonymous) {
			writer.name("anonymous");
			writer.value(true);
		}
	}

	@Override
	protected void deserialize(@NotNull GraphReader reader, @NotNull JsonObject nodeObject) throws GraphException {
		super.deserialize(reader, nodeObject);

		final JsonArray basesArray = expectNullableArray(nodeObject.get("bases"), "bases");
		if (basesArray != null) {
			for (final JsonElement baseElement : basesArray) {
				final JsonArray baseArray = expectArray(baseElement, "base");
				reader.readLinkAndSetLater(CppType.class, bases::add, baseArray);
			}
		}

		this.anonymous = expectNullableBoolean(nodeObject.get("anonymous"), "anonymous", false);
	}


	@MustBeInvokedByOverriders
	@Override
	protected int hash(@NotNull NodeHasher hasher) {
		int hash = super.hash(hasher);
		hash = hash * 31 + Boolean.hashCode(anonymous);
		hash = hash * 31 + (anonymous ? hasher.hashUnordered(getChildren()) : 1);
		return hash;
	}

	@MustBeInvokedByOverriders
	@Override
	protected boolean isSimilar(@NotNull NodeDiffer differ, @NotNull Node node) {
		if (!super.isSimilar(differ, node)) return false;
		final CppClassNode other = (CppClassNode) node;
		return anonymous == other.anonymous
				&& (!anonymous || differ.similarUnordered(getChildren(), other.getChildren()));
	}

	@MustBeInvokedByOverriders
	@Override
	protected boolean isIdentical(@NotNull NodeDiffer differ, @NotNull Node node) {
		if (!super.isIdentical(differ, node)) return false;
		final CppClassNode other = (CppClassNode) node;
		return anonymous == other.anonymous
				&& differ.similarUnordered(bases, other.bases)
				&& (!anonymous || differ.similarUnordered(getChildren(), other.getChildren()));
	}
}
