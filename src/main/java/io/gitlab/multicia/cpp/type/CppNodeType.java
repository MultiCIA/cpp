/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp.type;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.gitlab.multicia.cpp.node.CppNode;
import io.gitlab.multicia.graph.GraphException;
import io.gitlab.multicia.graph.GraphReader;
import io.gitlab.multicia.graph.GraphWriter;
import io.gitlab.multicia.graph.Node;
import io.gitlab.multicia.graph.NodeDiffer;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

import static io.gitlab.multicia.graph.GraphReader.expectNullableArray;

/**
 * Class Type / Enum Type / Struct Type / Union Type
 */
public final class CppNodeType extends CppType {
	private @Nullable CppNode node;


	public CppNodeType(@NotNull CppTypeGroup group) {
		super(group);
	}


	@Override
	protected @NotNull String getNodeClass() {
		return CppTypeGroup.CPP_NODE_TYPE;
	}


	public @Nullable CppNode getNode() {
		return node;
	}

	public void setNode(@Nullable CppNode node) {
		this.node = node;
	}


	@Override
	protected void serialize(@NotNull GraphWriter writer) throws IOException {
		super.serialize(writer);

		if (node != null) {
			writer.name("node");
			writer.writeLink(node);
		}
	}

	@Override
	protected void deserialize(@NotNull GraphReader reader, @NotNull JsonObject nodeObject) throws GraphException {
		super.deserialize(reader, nodeObject);

		final JsonArray nodeArray = expectNullableArray(nodeObject.get("node"), "node");
		if (nodeArray != null) {
			reader.readLinkAndSetLater(CppNode.class, this::setNode, nodeArray);
		}
	}


	@MustBeInvokedByOverriders
	@Override
	protected boolean isIdentical(@NotNull NodeDiffer differ, @NotNull Node node) {
		if (!super.isIdentical(differ, node)) return false;
		final CppNodeType other = (CppNodeType) node;
		return differ.isSimilar(this.node, other.node);
	}
}
