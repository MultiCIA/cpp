/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp.node;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.gitlab.multicia.fs.FileNode;
import io.gitlab.multicia.graph.GraphException;
import io.gitlab.multicia.graph.GraphReader;
import io.gitlab.multicia.graph.GraphWriter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

import static io.gitlab.multicia.graph.GraphReader.expectArray;
import static io.gitlab.multicia.graph.GraphReader.expectInt;
import static io.gitlab.multicia.graph.GraphReader.expectString;

public final class CppLocation {
	private @Nullable FileNode file;
	private int startLine;
	private int endLine;
	private @Nullable String content;


	public @Nullable FileNode getFile() {
		return file;
	}

	public void setFile(@Nullable FileNode file) {
		this.file = file;
	}

	public int getStartLine() {
		return startLine;
	}

	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}

	public @Nullable String getContent() {
		return content;
	}

	public void setContent(@Nullable String content) {
		this.content = content;
	}


	void serialize(@NotNull GraphWriter writer) throws IOException {
		writer.beginObject();
		if (file != null) {
			writer.name("file");
			writer.writeLink(file);
		}

		writer.name("lines");
		writer.beginArray();
		writer.value(startLine);
		writer.value(endLine);
		writer.endArray();

		if (content != null) {
			writer.name("content");
			writer.value(content);
		}
		writer.endObject();
	}

	void deserialize(@NotNull GraphReader reader, @NotNull JsonObject nodeObject) throws GraphException {
		final JsonArray fileArray = expectArray(nodeObject.get("file"), "file");
		reader.readLinkAndSetLater(FileNode.class, this::setFile, fileArray);
		final JsonArray linesArray = expectArray(nodeObject.get("lines"), "lines");
		if (linesArray.size() != 2) {
			throw new GraphException("Invalid json: lines should have only two elements!");
		}
		this.startLine = expectInt(linesArray.get(0), "startLine");
		this.endLine = expectInt(linesArray.get(1), "endLine");
		this.content = expectString(nodeObject.get("content"), "content");
	}
}
