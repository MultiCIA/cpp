/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp.type;

import io.gitlab.multicia.graph.Graph;
import io.gitlab.multicia.graph.GraphException;
import io.gitlab.multicia.graph.Group;
import org.jetbrains.annotations.NotNull;

/**
 * Cpp Type Group
 */
public final class CppTypeGroup extends Group<CppType> {
	public static final @NotNull String CPP_TYPE_GROUP = "CppType";
	public static final @NotNull String CPP_BASIC_TYPE = "BasicType";
	public static final @NotNull String CPP_FUNCTION_TYPE = "FunctionType";
	public static final @NotNull String CPP_NODE_TYPE = "NodeType";
	public static final @NotNull String CPP_TYPED_TYPE = "TypedType";


	public CppTypeGroup(@NotNull Graph graph) {
		super(graph);
	}


	@Override
	protected @NotNull String getGroupClass() {
		return CPP_TYPE_GROUP;
	}

	@Override
	protected @NotNull Class<CppType> getNodeClass() {
		return CppType.class;
	}

	@Override
	protected @NotNull CppType newNode(@NotNull String nodeClass) throws GraphException {
		switch (nodeClass) {
			case CPP_BASIC_TYPE:
				return new CppBasicType(this);
			case CPP_FUNCTION_TYPE:
				return new CppFunctionType(this);
			case CPP_NODE_TYPE:
				return new CppNodeType(this);
			case CPP_TYPED_TYPE:
				return new CppTypedType(this);
		}
		throw new GraphException("Cannot create node: Unknown node class!");
	}
}
