/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp.type;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.gitlab.multicia.graph.GraphException;
import io.gitlab.multicia.graph.GraphReader;
import io.gitlab.multicia.graph.GraphWriter;
import io.gitlab.multicia.graph.Node;
import io.gitlab.multicia.graph.NodeDiffer;
import io.gitlab.multicia.graph.NodeHasher;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.gitlab.multicia.graph.GraphReader.expectArray;
import static io.gitlab.multicia.graph.GraphReader.expectBoolean;
import static io.gitlab.multicia.graph.GraphReader.expectNullableArray;

/**
 * Function Type
 */
public final class CppFunctionType extends CppType {
	private @Nullable CppType returnType = null;
	private final @NotNull List<@NotNull CppType> parameters = new ArrayList<>();
	private boolean isVarArgs = false;


	public CppFunctionType(@NotNull CppTypeGroup group) {
		super(group);
	}


	@Override
	protected @NotNull String getNodeClass() {
		return CppTypeGroup.CPP_FUNCTION_TYPE;
	}


	public @Nullable CppType getReturnType() {
		return returnType;
	}

	public void setReturnType(@Nullable CppType returnType) {
		this.returnType = returnType;
	}


	public @NotNull List<@NotNull CppType> getParameters() {
		return Collections.unmodifiableList(parameters);
	}

	public void setParameters(@NotNull List<@NotNull CppType> parameters) {
		this.parameters.clear();
		this.parameters.addAll(parameters);
	}


	public boolean isVarArgs() {
		return isVarArgs;
	}

	public void setVarArgs(boolean varArgs) {
		isVarArgs = varArgs;
	}


	@Override
	protected void serialize(@NotNull GraphWriter writer) throws IOException {
		super.serialize(writer);

		if (returnType != null) {
			writer.name("returnType");
			writer.writeLink(returnType);
		}

		if (!parameters.isEmpty()) {
			writer.name("parameters");
			writer.beginArray();
			for (final CppType parameter : parameters) {
				writer.writeLink(parameter);
			}
			writer.endArray();
		}

		writer.name("varArgs");
		writer.value(isVarArgs);
	}

	@Override
	protected void deserialize(@NotNull GraphReader reader, @NotNull JsonObject nodeObject) throws GraphException {
		super.deserialize(reader, nodeObject);

		final JsonArray returnTypeArray = expectNullableArray(nodeObject.get("returnType"), "returnType");
		if (returnTypeArray != null) {
			reader.readLinkAndSetLater(CppType.class, this::setReturnType, returnTypeArray);
		}

		final JsonArray parametersArray = expectNullableArray(nodeObject.get("parameters"), "parameters");
		if (parametersArray != null) {
			for (final JsonElement parameterElement : parametersArray) {
				final JsonArray parameterArray = expectArray(parameterElement, "parameter");
				reader.readLinkAndSetLater(CppType.class, parameters::add, parameterArray);
			}
		}

		this.isVarArgs = expectBoolean(nodeObject.get("varArgs"), "varArgs");
	}


	@MustBeInvokedByOverriders
	@Override
	protected int hash(@NotNull NodeHasher hasher) {
		int hash = super.hash(hasher);
		hash = hash * 31 + hasher.hash(returnType);
		hash = hash * 31 + hasher.hashOrdered(parameters);
		hash = hash * 31 + (isVarArgs ? 1231 : 1237);
		return hash;
	}

	@MustBeInvokedByOverriders
	@Override
	protected boolean isSimilar(@NotNull NodeDiffer differ, @NotNull Node node) {
		if (!super.isSimilar(differ, node)) return false;
		final CppFunctionType other = (CppFunctionType) node;
		return isVarArgs == other.isVarArgs
				&& differ.isSimilar(returnType, other.returnType)
				&& differ.similarOrdered(parameters, other.parameters);
	}

	@MustBeInvokedByOverriders
	@Override
	protected boolean isIdentical(@NotNull NodeDiffer differ, @NotNull Node node) {
		if (!super.isIdentical(differ, node)) return false;
		final CppFunctionType other = (CppFunctionType) node;
		return isVarArgs == other.isVarArgs
				&& differ.isSimilar(returnType, other.returnType)
				&& differ.similarOrdered(parameters, other.parameters);
	}
}
