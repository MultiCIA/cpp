/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp.node;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.gitlab.multicia.cpp.type.CppType;
import io.gitlab.multicia.graph.GraphException;
import io.gitlab.multicia.graph.GraphReader;
import io.gitlab.multicia.graph.GraphWriter;
import io.gitlab.multicia.graph.Node;
import io.gitlab.multicia.graph.NodeDiffer;
import io.gitlab.multicia.graph.NodeHasher;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Objects;

import static io.gitlab.multicia.graph.GraphReader.expectNullableArray;
import static io.gitlab.multicia.graph.GraphReader.expectNullableString;

/**
 * Variable / Field / Template Parameter / Enumerator
 */
public final class CppVariableNode extends CppNode {
	private @Nullable CppType type = null;
	private @Nullable String value = null;


	public CppVariableNode(@NotNull CppGroup group) {
		super(group);
	}

	@Override
	protected @NotNull String getNodeClass() {
		return CppGroup.CPP_VARIABLE_NODE;
	}


	public @Nullable CppType getType() {
		return type;
	}

	public void setType(@Nullable CppType type) {
		this.type = type;
	}


	public @Nullable String getValue() {
		return value;
	}

	public void setValue(@Nullable String value) {
		this.value = value;
	}


	@Override
	protected void serialize(@NotNull GraphWriter writer) throws IOException {
		super.serialize(writer);

		if (type != null) {
			writer.name("type");
			writer.writeLink(type);
		}
		if (value != null) {
			writer.name("value");
			writer.value(value);
		}
	}

	@Override
	protected void deserialize(@NotNull GraphReader reader, @NotNull JsonObject nodeObject) throws GraphException {
		super.deserialize(reader, nodeObject);

		final JsonArray typeArray = expectNullableArray(nodeObject.get("type"), "type");
		if (typeArray != null) {
			reader.readLinkAndSetLater(CppType.class, this::setType, typeArray);
		}

		this.value = expectNullableString(nodeObject.get("value"), "value");
	}


	@MustBeInvokedByOverriders
	@Override
	protected int hash(@NotNull NodeHasher hasher) {
		int hash = super.hash(hasher);
		hash = hash * 31 + hasher.hash(type);
		return hash;
	}

	@MustBeInvokedByOverriders
	@Override
	protected boolean isSimilar(@NotNull NodeDiffer differ, @NotNull Node node) {
		if (!super.isSimilar(differ, node)) return false;
		final CppVariableNode other = (CppVariableNode) node;
		return differ.isSimilar(type, other.type);
	}

	@MustBeInvokedByOverriders
	@Override
	protected boolean isIdentical(@NotNull NodeDiffer differ, @NotNull Node node) {
		if (!super.isIdentical(differ, node)) return false;
		final CppVariableNode other = (CppVariableNode) node;
		return Objects.equals(value, other.value)
				&& differ.isSimilar(type, other.type);
	}
}
