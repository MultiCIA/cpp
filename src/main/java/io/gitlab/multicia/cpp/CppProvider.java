/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp;

import io.gitlab.multicia.cpp.node.CppGroup;
import io.gitlab.multicia.cpp.type.CppTypeGroup;
import io.gitlab.multicia.graph.Connection;
import io.gitlab.multicia.graph.Graph;
import io.gitlab.multicia.graph.GraphException;
import io.gitlab.multicia.graph.Group;
import io.gitlab.multicia.graph.Provider;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

public enum CppProvider implements Provider {
	INSTANCE;

	private static final @NotNull Set<String> GROUPS = Set.of(CppGroup.CPP_GROUP, CppTypeGroup.CPP_TYPE_GROUP);
	private static final @NotNull Set<String> CONNECTIONS
			= Set.of(CppConnection.VALUES.stream().map(Connection::getName).toArray(String[]::new));


	@Override
	public @NotNull Group<?> newGroup(@NotNull Graph graph, @NotNull String groupClass) throws GraphException {
		switch (groupClass) {
			case CppGroup.CPP_GROUP:
				return new CppGroup(graph);
			case CppTypeGroup.CPP_TYPE_GROUP:
				return new CppTypeGroup(graph);
		}
		throw new GraphException("Cannot create group: Unknown group class!");
	}

	@Override
	public @NotNull Set<String> getProvidedGroups() {
		return GROUPS;
	}

	@Override
	public @NotNull Connection newConnection(@NotNull String connectionClass) throws GraphException {
		for (final CppConnection connection : CppConnection.VALUES) {
			if (connectionClass.equals(connection.getName())) {
				return connection;
			}
		}
		throw new GraphException("Cannot create connection: Unknown connection class!");
	}

	@Override
	public @NotNull Set<String> getProvidedConnections() {
		return CONNECTIONS;
	}
}
