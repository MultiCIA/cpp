/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp.helper;

import io.gitlab.multicia.cpp.CppBuilder;
import io.gitlab.multicia.cpp.CppBuilderException;
import io.gitlab.multicia.cpp.CppProvider;
import io.gitlab.multicia.fs.FileSystemProvider;
import io.gitlab.multicia.graph.Graph;
import io.gitlab.multicia.graph.Provider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.List;

public final class CppProjectBuilder {
	private static final Logger LOGGER = LoggerFactory.getLogger(CppProjectBuilder.class);


	public static final List<Provider> CPP_PROVIDERS = List.of(CppProvider.INSTANCE, FileSystemProvider.INSTANCE);


	private CppProjectBuilder() {
	}


	public static @NotNull Graph buildQtProject(@NotNull Path projectPath, @NotNull Path projectFile,
			@Nullable Path gppPath, @Nullable Path qmakePath, @Nullable Path makePath, int jobsCount)
			throws IOException, CppBuilderException {
		LOGGER.info("Start building Qt project...");
		try {
			final Path projectRealPath = projectPath.toRealPath(LinkOption.NOFOLLOW_LINKS);
			final Path projectRealFile = projectRealPath.resolve(projectFile).toRealPath(LinkOption.NOFOLLOW_LINKS);
			final Path gppRealPath = gppPath != null ? gppPath.toRealPath(LinkOption.NOFOLLOW_LINKS) : null;
			final Path qmakeRealPath = qmakePath != null ? qmakePath.toRealPath(LinkOption.NOFOLLOW_LINKS) : null;
			final Path makeRealPath = makePath != null ? makePath.toRealPath(LinkOption.NOFOLLOW_LINKS) : null;
			final CppEnvironmentParser parser = CppEnvironmentParser.parseQtProjectFile(projectRealFile,
					gppRealPath, qmakeRealPath, makeRealPath, jobsCount);
			return CppBuilder.build(parser.getPredefinedMacros(), parser.getIncludePaths(),
					projectRealPath, parser.getProjectSources(), parser.getProjectHeaders());
		} catch (final IOException | CppBuilderException exception) {
			LOGGER.error("Failed building Qt project with exception!", exception);
			throw exception;
		} finally {
			LOGGER.info("End building Qt project.");
		}
	}

	public static @NotNull Graph buildGppProject(@NotNull Path projectFolderPath, @Nullable Path gppPath)
			throws IOException, CppBuilderException {
		LOGGER.info("Start building C++ project...");
		try {
			final Path projectRealPath = projectFolderPath.toRealPath(LinkOption.NOFOLLOW_LINKS);
			final Path gppRealPath = gppPath != null ? gppPath.toRealPath(LinkOption.NOFOLLOW_LINKS) : null;
			final CppEnvironmentParser parser = CppEnvironmentParser.parseGppProject(projectRealPath, gppRealPath);
			return CppBuilder.build(parser.getPredefinedMacros(), parser.getIncludePaths(),
					projectRealPath, parser.getProjectSources(), parser.getProjectHeaders());
		} catch (final IOException | CppBuilderException exception) {
			LOGGER.error("Failed building C++ project with exception!", exception);
			throw exception;
		} finally {
			LOGGER.info("End building C++ project.");
		}
	}
}
