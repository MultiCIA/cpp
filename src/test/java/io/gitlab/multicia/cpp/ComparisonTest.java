/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp;

import io.gitlab.multicia.cpp.helper.CppProjectBuilder;
import io.gitlab.multicia.graph.Connection;
import io.gitlab.multicia.graph.Graph;
import io.gitlab.multicia.graph.GraphChange;
import io.gitlab.multicia.graph.GraphDiffer;
import io.gitlab.multicia.graph.GraphDifference;
import io.gitlab.multicia.graph.GraphHelper;
import io.gitlab.multicia.graph.Node;
import org.eclipse.collections.api.map.primitive.ObjectDoubleMap;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.api.tuple.primitive.ObjectDoublePair;
import org.eclipse.collections.impl.map.mutable.primitive.ObjectDoubleHashMap;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Path;

public final class ComparisonTest {
	public static void main(@NotNull String[] args) throws CppBuilderException, IOException {
		final long start = System.currentTimeMillis();
		try {
			final Path projectPath = Path.of("./local/cpp-impact-test/Test15");

			final Path projectPathA = projectPath.resolve("old");
			final Path projectJsonA = projectPathA.resolve("output.json");
			final Graph graphA = CppProjectBuilder.buildGppProject(projectPathA, null);
			GraphHelper.graphToFile(projectJsonA, graphA);

			final Path projectPathB = projectPath.resolve("new");
			final Path projectJsonB = projectPathB.resolve("output.json");
			final Graph graphB = CppProjectBuilder.buildGppProject(projectPathB, null);
			GraphHelper.graphToFile(projectJsonB, graphB);

			final GraphDiffer graphDiffer = new GraphDiffer(graphA, graphB);
			final GraphDifference difference = graphDiffer.compare();
			final ObjectDoubleMap<Node> impact = difference.calculateImpact(
					new ObjectDoubleHashMap<Connection>()
							.withKeyValue(CppConnection.USE, 0.5)
							.withKeyValue(CppConnection.MEMBER, 0.5)
							.withKeyValue(CppConnection.INHERIT, 0.5)
							.withKeyValue(CppConnection.CALL, 0.5)
							.withKeyValue(CppConnection.OVERRIDE, 0.5)
			);
			for (final ObjectDoublePair<Node> pair : impact.keyValuesView()) {
				pair.getOne().setImpact(pair.getTwo());
			}
			for (final Node node : difference.createAddedList()) {
				node.setChange(GraphChange.ADDED);
			}
			for (final Pair<Node, Node> pair : difference.createChangedList()) {
				pair.getTwo().setChange(GraphChange.CHANGED);
			}
			for (final Pair<Node, Node> pair : difference.createUnchangedList()) {
				pair.getTwo().setChange(GraphChange.UNCHANGED);
			}

			final Path projectComparisonJson = projectPath.resolve("comparison.json");
			GraphHelper.graphToFile(projectComparisonJson, graphB);
		} finally {
			System.out.println((System.currentTimeMillis() - start) + " milliseconds");
		}
	}
}
