/*
 * Copyright (C) 2021-2022 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.cpp.node;

import io.gitlab.multicia.cpp.type.CppFunctionType;
import io.gitlab.multicia.cpp.type.CppType;
import io.gitlab.multicia.graph.Node;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Set;
import java.util.stream.Collectors;

public final class CppNodeHelper {
	private static final @NotNull Set<Class<? extends CppNode>> CONTAINER_NODE_TYPES = Set.of(
			CppClassNode.class,
			CppEnumerationNode.class,
			CppNamespaceNode.class,
			CppStructNode.class,
			CppUnionNode.class
	);

	private CppNodeHelper() {
	}


	public static @NotNull String readableName(@NotNull CppNode node) {
		if (node instanceof CppFunctionNode) {
			return readableName((CppFunctionNode) node).trim().replaceAll("\\s+", " ");
		} else if (node instanceof CppVariableNode) {
			return readableName((CppVariableNode) node).trim().replaceAll("\\s+", " ");
		} else {
			return qualifiedName(node);
		}
	}

	public static @NotNull String readableName(@NotNull CppFunctionNode node) {
		final CppFunctionType type = node.getType();
		if (type == null) return qualifiedName(node);
		final StringBuilder builder = new StringBuilder();
		final CppType returnType = type.getReturnType();
		if (returnType != null) builder.append(returnType.getName()).append(' ');
		builder.append(qualifiedName(node)).append('(').append(
				type.getParameters().stream().map(CppType::getName).collect(Collectors.joining(","))
		).append(')');
		return builder.toString();
	}

	public static @NotNull String readableName(@NotNull CppVariableNode node) {
		final CppType type = node.getType();
		return type != null ? type.getName() + ' ' + qualifiedName(node) : qualifiedName(node);
	}

	public static @NotNull String qualifiedName(@NotNull CppNode node) {
		final Deque<CppNode> nodes = new ArrayDeque<>();
		CppNode current = node;
		while (true) {
			final Node parent = current.getParent();
			if (parent instanceof CppNode && CONTAINER_NODE_TYPES.contains(parent.getClass())) {
				current = (CppNode) parent;
				nodes.push(current);
			} else {
				break;
			}
		}
		final StringBuilder builder = new StringBuilder();
		while (true) {
			final CppNode cppNode = nodes.pollFirst();
			if (cppNode == null) break;
			builder.append(cppNode.getName()).append("::");
		}
		builder.append(node.getName());
		return builder.toString();
	}
}
